import React, { Component } from 'react';
import {
  Button,
  DrawerLayoutAndroid,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import { DrawerNavigator, StackNavigator } from 'react-navigation';

import navIcon from './images/icon_navigation.png';

class HomeScreen extends Component {
  static navigationOptions = {
    title: 'Welcome',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View>
        <Text>Page 1</Text>
        <Button
          onPress={() => navigate('PageB')}
          title='Go to Page B'
        />
      </View>
    );
  }
}

class PageB extends Component {
  constructor () {
    super();
    this.openDrawer = this.openDrawer.bind(this);
  }

  openDrawer() {
    this.drawer.openDrawer();
  }

  render() {
    const navigationView = (
      <View style={styles.navView}>
        <Text style={styles.navStyle}>Test 1</Text>
        <Text style={styles.navStyle}>Test 2</Text>
        <Text style={styles.navStyle}>Test 3</Text>
      </View>
    );

    return (
      <DrawerLayoutAndroid
        drawerWidth={300}
        ref={(_drawer) => this.drawer = _drawer}
        drawerPosition={DrawerLayoutAndroid.positions.Left}
        renderNavigationView={() => navigationView}>
        <View style={{marginTop: 40}}>
          <TouchableHighlight onPress={this.openDrawer}>
            <Image style={styles.navIcon}
              source={require('./images/icon_navigation.png')} />
          </TouchableHighlight>
        </View>
      </DrawerLayoutAndroid>
    );
  }
}

const SimpleApp = StackNavigator({
  Home: { screen: HomeScreen },
  PageB: { screen: PageB },
});

export default class App extends React.Component {
  render() {
    return <SimpleApp />;
  }
}

const styles = StyleSheet.create({
  navIcon: {
    width: 34,
    height: 28
  },
  navStyle: {
    marginLeft: 10,
    fontSize: 15,
    textAlign: 'left'
  },
  navView: {
    marginTop: 100,
    flex: 1,
    backgroundColor: '#fff'
  }
});
